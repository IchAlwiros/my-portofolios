import Github from "./image/github.png";
import Linkedin from "./image/linkedin.png";
import Instagram from "./image/instagram.png";
import Vektor1 from "./image/Vector1.png";
import Vektor2 from "./image/Vector2.png";
import Chrown from "./image/crown.png";
import Facebook from "./image/Facebook.png";
import Wave from "./image/wave.png";
import Boy1 from "./image/boy1.png";
import Profile1 from "./image/profile.png";
import Elnusa from "./image/elnusa.png";
import Tri from "./image/logorbs.png";
import Iconplus from "./image/iconplus.png";
import Yazid from "./image/yazid.png";
import Alwiros from "./image/alwiros.png";
import Item1 from "./image/item1.png";
import JsIcon from "./image/jsicon.png";
import APIcon from "./image/apis.png";
import ReactIcon from "./image/react.png";
import FigmaIcon from "./image/figma.png";
import Porto1 from "./image/swagger.png";
import Porto2 from "./image/porto2.png";
import Porto3 from "./image/uinyervis.png";
import Porto4 from "./image/porto4.jpg";
import Porto5 from "./image/porto5.png";
import Porto6 from "./image/swagger2.png";
import Porto7 from "./image/Sc1.png";
import Porto8 from "./image/porto8.png";
import Porto9 from "./image/porto9.png";
import Porto10 from "./image/porto10.png";

export {
  Github,
  Linkedin,
  Instagram,
  Vektor1,
  Vektor2,
  Chrown,
  Facebook,
  Wave,
  Boy1,
  Profile1,
  Elnusa,
  Iconplus,
  Yazid,
  Tri,
  Alwiros,
  Item1,
  JsIcon,
  APIcon,
  ReactIcon,
  FigmaIcon,
  Porto1,
  Porto2,
  Porto3,
  Porto4,
  Porto5,
  Porto6,
  Porto7,
  Porto8,
  Porto9,
  Porto10,
};
