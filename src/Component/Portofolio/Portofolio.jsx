import "./Portofolio.css";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y, EffectCube } from "swiper";
import "swiper/swiper-bundle.min.css";

import MediaQuery from "react-responsive";
import "swiper/css";
// import MediaQuery from 'react-responsive'
import {
  Porto1,
  Porto2,
  Porto3,
  Porto4,
  Porto5,
  Porto6,
  Porto7,
  Porto8,
  Porto9,
  Porto10,
} from "../../Assets/Index";

import { ThemeContext } from "../../Context/Context";
import { useContext } from "react";

import Klik from "@iconscout/react-unicons/icons/uil-external-link-alt";
import Git from "@iconscout/react-unicons/icons/uil-gitlab";

export const Portofolio = () => {
  const theme = useContext(ThemeContext);
  const darkMode = theme.state.darkMode;

  // const isMobileDevice = useMediaQuery({
  //     query: "(min-device-width: 480px)",
  //   });

  return (
    <div className="portfolio" id="Portofolio">
      {/* heading */}
      <span style={{ color: darkMode ? "white" : "" }}>Recent Projects</span>
      <span>Portfolio</span>

      {/* Mobile */}
      <MediaQuery maxDeviceWidth={480}>
        <Swiper
          spaceBetween={30}
          slidesPerView={1}
          grabCursor={true}
          modules={[Navigation, Pagination, Scrollbar, A11y, EffectCube]}
          navigation
          pagination={{ clickable: true }}
          className="portfolio-slider"
        >
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://backend-gateway.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto1} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>REST API ECOMERCE</h3>

                <p>
                  REST API is used to manage online stores or ecommerce sites.
                  consists of several services such as products, orders, and
                  also chat buyers
                </p>
                <div className="stack">
                  <p>Express</p>
                  <p>Swagger</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://backend-gateway.vercel.app/api-docs/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://backend-gateway.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto7} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>GOTRONIC ECOMERCE</h3>

                <p>
                  A site for buying and selling electronic products and gadgets
                  The website provides an interface for searching, comparing,
                  ordering and buying electronic products.
                </p>
                <div className="stack">
                  <p>React</p>
                  <p>Ant Design</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://gotronic.vercel.app/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://express-gabuts.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto6} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>REST API CRUD ARTICLE</h3>

                <p>
                  REST API is used to manage items to perform CRUD (Create,
                  Read, Update, Delete) operations with case study articles.
                  consists of several services such as article (CRUD),
                  authentification (jwt)
                </p>
                <div className="stack">
                  <p>Express</p>
                  <p>Swagger</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://express-gabuts.vercel.app/api-docs/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://frontend-technoverse.vercel.app/"
                  rel="noreferrer"
                >
                  <img src={Porto8} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>TEKNOVERSE</h3>

                <p>
                  A website that manages store product needs. Its main feature
                  is the barcode scan which is used to search for products.
                  Another feature is (CRUD) product management
                </p>
                <div className="stack">
                  <p>Antd Design</p>
                  <p>React Js</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://frontend-technoverse.vercel.app/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://ichal-todolist-app-devrank.vercel.app/"
                  rel="noreferrer"
                >
                  <img src={Porto10} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>TODOLIST APP</h3>

                <p>
                  A website that manages activity user. Its main feature is
                  implementation of CRUD with ReactJS. this app is support API
                  from gethired.
                </p>
                <div className="stack">
                  <p>Antd Design</p>
                  <p>React Js</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://ichal-todolist-app-devrank.vercel.app/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://express-gabuts.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto9} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>TEKNOVERSE RESTAPI</h3>

                <p>
                  This REST API has standard features, namely CRUD products,
                  scan code, then auth operations with jwt
                </p>
                <div className="stack">
                  <p>Express Js</p>
                  <p>Swagger</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://backend-tecnoverse.vercel.app/api-docs/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://www.behance.net/gallery/141783583/Nyayur-Mobile-apps"
                  rel="noreferrer"
                >
                  <img src={Porto2} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>UI DESIGN NYAYUR</h3>

                <p>
                  Android mobile app UI design. provides an overview of the UI
                  for apps that manage farm trades. provides buying and selling
                  vegetables, garden tools, and hydroponic installation services
                  and others
                </p>
                <div className="stack">
                  <p>Figma</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://www.behance.net/gallery/141783583/Nyayur-Mobile-apps"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://www.behance.net/gallery/141783583/Nyayur-Mobile-apps"
                  rel="noreferrer"
                >
                  <img src={Porto3} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>UI DESIGN NYERVIS</h3>

                <p>
                  Android mobile app UI design. provides an overview of the UI
                  for apps that manage service recruitment. providing rental
                  services, carpentry materials, and emergency installation
                  services and others
                </p>
                <div className="stack">
                  <p>Figma</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://www.behance.net/gallery/141783583/Nyayur-Mobile-apps"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </MediaQuery>

      {/* Dekstop */}
      <MediaQuery minDeviceWidth={480}>
        <Swiper
          spaceBetween={30}
          slidesPerView={1}
          grabCursor={true}
          modules={[Navigation, Pagination, Scrollbar, A11y, EffectCube]}
          navigation
          pagination={{ clickable: true }}
          className="portfolio-slider"
        >
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://backend-gateway.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto1} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>REST API ECOMERCE</h3>

                <p>
                  REST API is used to manage online stores or ecommerce sites.
                  consists of several services such as products, orders, and
                  also chat buyers
                </p>
                <div className="stack">
                  <p>Express</p>
                  <p>Swagger</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://backend-gateway.vercel.app/api-docs/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://backend-gateway.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto7} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>GOTRONIC ECOMERCE</h3>

                <p>
                  A site for buying and selling electronic products and gadgets
                  The website provides an interface for searching, comparing,
                  ordering and buying electronic products.
                </p>
                <div className="stack">
                  <p>React</p>
                  <p>Ant Design</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://gotronic.vercel.app/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://express-gabuts.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto6} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>REST API CRUD ARTICLE</h3>

                <p>
                  REST API is used to manage items to perform CRUD (Create,
                  Read, Update, Delete) operations with case study articles.
                  consists of several services such as article (CRUD),
                  authentification (jwt)
                </p>
                <div className="stack">
                  <p>Express</p>
                  <p>Swagger</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://express-gabuts.vercel.app/api-docs/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://frontend-technoverse.vercel.app/"
                  rel="noreferrer"
                >
                  <img src={Porto8} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>TEKNOVERSE</h3>

                <p>
                  A website that manages store product needs. Its main feature
                  is the barcode scan which is used to search for products.
                  Another feature is (CRUD) product management
                </p>
                <div className="stack">
                  <p>Antd Design</p>
                  <p>React Js</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://frontend-technoverse.vercel.app/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://ichal-todolist-app-devrank.vercel.app/"
                  rel="noreferrer"
                >
                  <img src={Porto10} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>TODOLIST APPS</h3>

                <p>
                  A website that manages activity user. Its main feature is
                  implementation of CRUD with ReactJS. this app is support API
                  from gethired.
                </p>
                <div className="stack">
                  <p>Antd Design</p>
                  <p>React Js</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://ichal-todolist-app-devrank.vercel.app/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://backend-tecnoverse.vercel.app/api-docs/"
                  rel="noreferrer"
                >
                  <img src={Porto9} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>TEKNOVERSE RESTAPI</h3>

                <p>
                  This REST API has standard features, namely CRUD products,
                  scan code, then auth operations with jwt
                </p>
                <div className="stack">
                  <p>Express Js</p>
                  <p>Swagger</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://gitlab.com/IchAlwiros"
                    rel="noreferrer"
                  >
                    Code&nbsp;
                    <Git size={22} />
                  </a>
                  <a
                    target="_blank"
                    href="https://backend-tecnoverse.vercel.app/api-docs/"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://www.behance.net/gallery/141783583/Nyayur-Mobile-apps"
                  rel="noreferrer"
                >
                  <img src={Porto2} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>UI DESIGN NYAYUR</h3>

                <p>
                  Android mobile app UI design. provides an overview of the UI
                  for apps that manage farm trades. provides buying and selling
                  vegetables, garden tools, and hydroponic installation services
                  and others
                </p>
                <div className="stack">
                  <p>Figma</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://www.behance.net/gallery/141783583/Nyayur-Mobile-apps"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div
              className="container-por"
              style={{
                backgroundColor: darkMode ? "white" : "",
                color: darkMode ? "black" : "",
              }}
            >
              <div className="pro__img">
                <a
                  target="_blank"
                  href="https://www.behance.net/gallery/142684219/Nyervis-Apps"
                  rel="noreferrer"
                >
                  <img src={Porto3} alt="website" />
                </a>
              </div>
              <div className="pro_text">
                <h3>UI DESIGN NYERVIS</h3>

                <p>
                  Android mobile app UI design. provides an overview of the UI
                  for apps that manage service recruitment. providing rental
                  services, carpentry materials, and emergency installation
                  services and others
                </p>
                <div className="stack">
                  <p>Figma</p>
                </div>
                <div className="links">
                  <a
                    target="_blank"
                    href="https://www.behance.net/gallery/142684219/Nyervis-Apps"
                    rel="noreferrer"
                  >
                    Live Demo&nbsp;
                    <Klik size={20} />
                  </a>
                </div>
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </MediaQuery>
    </div>
  );
};
